#+AUTHOR: gandalfdwite
#+EMAIL: pravarag@gmail.com
#+TAGS: dev ops read meeting
* GOALS
** DevOps
*** TODO Kubernetes Best Practices by Brendan Burns
    :PROPERTIES:
    :ESTIMATED: 50
    :ACTUAL:
    :OWNER: gandalfdwite
    :ID: READ.1599058922
    :TASKID: READ.1599058922
    :END:
*** TODO Container Networking for Docker & Kubernetes
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:
    :OWNER: gandalfdwite
    :ID: READ.1599039494
    :TASKID: READ.1599039494
    :END:
    - [ ] Container Networking e-book by Oreilly ( 5h )
** Python
** Go Language
** Emacs
** Linux
*** TODO Unix and Linux system adminstration handbook [0/31]
    :PROPERTIES:
    :ESTIMATED: 72
    :ACTUAL:
    :OWNER: gandalfdwite
    :ID: READ.1593841714
    :TASKID: READ.1593841714
    :END:
    - [ ] Where to Start                   ( 2h )
    - [ ] Booting & sys management Daemons ( 2h )
    - [ ] Access Control & Rootly Powers   ( 2h )
    - [ ] Process Control                  ( 2h )
    - [ ] The Filesystem                   ( 2h )
    - [ ] Software Installation and Management ( 2h )
    - [ ] Scripting and the shell          ( 2h )
    - [ ] User Management                  ( 2h )
    - [ ] Cloud Computing                  ( 2h )
    - [ ] Logging                          ( 2h )
    - [ ] Drivers and the kernel           ( 2h )
    - [ ] Printing                         ( 2h )
    - [ ] TCP/IP Networking                ( 4h )
    - [ ] Physical Networking              ( 2h )
    - [ ] IP Routing                       ( 2h )
    - [ ] DNS                              ( 4h )
    - [ ] SSO                              ( 2h )
    - [ ] Email                            ( 4h )
    - [ ] Web Hosting                      ( 3h )
    - [ ] Storage                          ( 4h )
    - [ ] NFS                              ( 2h )
    - [ ] SMB                              ( 1h )
    - [ ] Config Management                ( 4h )
    - [ ] Virtualization                   ( 1h )
    - [ ] Containers                       ( 2h )
    - [ ] CI/CD                            ( 2h )
    - [ ] Security                         ( 4h )
    - [ ] Monitoring                       ( 2h )
    - [ ] Perf Analysis                    ( 2h )
    - [ ] Data Center Basics               ( 1h )
    - [ ] Methodology, Policy and Politics ( 2h )

*** TODO Git Pocket Guide [0/14]
    :PROPERTIES:
    :ESTIMATED: 30
    :ACTUAL:
    :OWNER: gandalfdwite
    :ID: READ.1593841309
    :TASKID: READ.1593841309
    :END:
    - [ ] Understanding Git     ( 2h )
    - [ ] Getting started       ( 2h )
    - [ ] Making Commits        ( 2h )
    - [ ] Undoing & Editing     ( 2h )
    - [ ] Branching             ( 3h )
    - [ ] Tracking Other Repositories ( 3h )
    - [ ] Merging               ( 2h )
    - [ ] Naming Commits        ( 2h )
    - [ ] Viewing History       ( 2h )
    - [ ] Editing History       ( 2h )
    - [ ] Understanding patches ( 2h )
    - [ ] Remote Access         ( 2h )
    - [ ] Miscellaneous         ( 2h )
    - [ ] How Do I?             ( 2h )
** Database
** Projects
** Blog - pravaragdotcom
* PLAN

